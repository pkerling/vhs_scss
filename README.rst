Introduction
============

This extension compiles scss vhs assets to css with scssphp.

Installation
============

* Composer: Run ``composer req pkerling/vhs_scss``
* Via TER: Install ``vhs_scss``
* From Git: Run ``composer install`` in the ``Resources/Private`` directory in order to install
  scssphp as dependency. Then install as TYPO3 extension in the backend.

No further configuration is necessary.

Usage
=====

Example usage in template:

::

    {namespace scss=Casix\VhsScss\ViewHelpers}
    <!-- basic -->
    <scss:asset.scssStyle path="fileadmin/main.scss" />
    <!-- advanced, note that enabling source map will include all SCSS sources in the output -->
    <scss:asset.scssStyle path="fileadmin/main.scss" scssVariables="{border: '20px'}" scssphpOutputStyle="expanded" generateSourceMap="TRUE" />

Example usage in PHP:

::

    $asset = \Casix\VhsScss\ScssAsset::getInstance();
    $asset->setName('testAsset1');
    $asset->setPath('EXT:user_site/Resources/Private/Scss/test.scss');
    $asset->setScssVariables(['bgColor' => 'yellow']);
    $asset->setScssphpOutputStyle('compressed');
    $asset->finalize();

The scss file will be compiled and integrated with the rest of the css
according to your vhs asset configuration. Scss compilation is cached.
The cache is invalidated when any used scss file or the options are
modified.

Configuration options in TypoScript (lower in priority than arguments
specified on the ViewHelper):

::

    plugin.tx_vhsscss.settings {
        # Output style to use, see http://scssphp.github.io/scssphp/docs/
        outputStyle = compressed
        # Arbitrary variables to set in the SCSS context
        variables {
            border-width = 40px
        }
        # Whether to include source maps and sources in the generated CSS
        generateSourceMap = 1
    }

Upgrading
=========

Breaking changes from v1 to v2
------------------------------
* ``scssphpFormatter`` setting replaced with ``scssphpOutputStyle`` due to changes in scssphp. You
  now need to use a supported output style (as of scssphp v1.4, ``compressed`` or ``expanded``)
  instead of a formatter class name.
* ``scssLinenumbers`` setting removed due to deprecation in scssphp.