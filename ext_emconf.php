<?php
$EM_CONF[$_EXTKEY] = array(
  'title' => 'VHS SCSS integration',
  'description' => 'Integrate SCSS template compilation into the vhs asset pipeline',
  'category' => 'fe',
  'author' => 'Philipp Kerling',
  'author_email' => 'pkerling@casix.org',
  'state' => 'beta',
  'uploadfolder' => 0,
  'createDirs' => '',
  'clearCacheOnLoad' => 1,
  'version' => '2.0.3',
  'constraints' =>
  array(
    'depends' =>
    array(
      'vhs' => '4.0.0-6.99.99',
      'typo3' => '8.7.0-10.4.99',
    ),
    'conflicts' =>
    array(),
    'suggests' =>
    array(),
  ),
);
