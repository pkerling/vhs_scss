<?php
defined('TYPO3_MODE') or die();

(function() {
    $ext = 'vhs_scss';
    // Support for non-composer installation
    $autoloadFile = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($ext, 'Resources/Private/Libraries/autoload.php');
    if (file_exists($autoloadFile)) {
        require_once($autoloadFile);
    }

    if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$ext])) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$ext] = [];
    }
    if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$ext]['options']['defaultLifetime'])) {
        // Default lifetime: 1 week
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$ext]['options']['defaultLifetime'] = 7 * 24 * 60 * 60;
    }
})();
