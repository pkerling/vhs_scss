<?php

declare(strict_types=1);

namespace Casix\VhsScss;

/*
 * This file is part of the vhs_scss project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use FluidTYPO3\Vhs\Asset;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

class ScssAsset extends Asset
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var array
     */
    protected $scssVariables = [];

    /**
     * @var string|null
     */
    protected $scssphpOutputStyle = null;

    /**
     * @var bool
     */
    protected $generateSourceMap = false;

    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->setType('css');
    }

    public static function getInstance(): ScssAsset
    {
        $asset = GeneralUtility::makeInstance(ObjectManager::class)->get(ScssAsset::class);
        return $asset;
    }

    public static function createFromSettings(array $settings): ScssAsset
    {
        $asset = self::getInstance();
        foreach ($settings as $propertyName => $value) {
            ObjectAccess::setProperty($asset, $propertyName, $value);
        }
        $asset->setType('css');
        return $asset->finalize();
    }

    /**
     * @param string $filePathAndFilename
     * @return ScssAsset
     */
    public static function createFromFile($filePathAndFilename): ScssAsset
    {
        $asset = self::getInstance();
        $asset->setExternal(false);
        $asset->setPath($filePathAndFilename);
        $asset->setType('css');
        return $asset->finalize();
    }

    /**
     * @param string $content
     * @return ScssAsset
     */
    public static function createFromContent($content): ScssAsset
    {
        $asset = self::getInstance();
        $asset->setContent($content);
        $asset->setName(md5($content));
        $asset->setType('css');
        return $asset->finalize();
    }

    public function setScssVariables(array $variables): void
    {
        $this->scssVariables = $variables;
    }

    public function getScssVariables(): array
    {
        return $this->scssVariables;
    }

    public function setScssphpOutputStyle(string $scssphpOutputStyle): void
    {
        $this->scssphpOutputStyle = $scssphpOutputStyle;
    }

    public function getScssphpOutputStyle(): ?string
    {
        return $this->scssphpOutputStyle;
    }

    public function setGenerateSourceMap(bool $generateSourceMap): void
    {
        $this->generateSourceMap = $generateSourceMap;
    }

    public function getGenerateSourceMap(): bool
    {
        return $this->generateSourceMap;
    }

    public function build(): string
    {
        /** @var Compiler $compiler */
        $compiler = $this->objectManager->get(Compiler::class);
        $compiler->configureFromTypoScript();
        $settings = $this->getSettings();
        if (isset($settings['scssVariables']) && is_array($settings['scssVariables'])) {
            $compiler->setVariables($settings['scssVariables']);
        }
        if (!empty($settings['scssphpOutputStyle'] ?? '')) {
            $compiler->setOutputStyle($settings['scssphpOutputStyle']);
        }
        if (isset($settings['generateSourceMap'])) {
            $compiler->setGenerateSourceMap((bool)$settings['generateSourceMap']);
        }
        $this->setSourceOnCompiler($compiler);
        return $compiler->compile();
    }

    protected function setSourceOnCompiler(Compiler $compiler): void
    {
        if (false !== $this->getExternal()) {
            throw new \Exception('SCSS files cannot be included from external sources', 1518790785);
        } else if (!empty($this->getPath())) {
            $compiler->setScssFile($this->getPath());
        } else {
            $compiler->setScss($this->getContent());
        }
    }
}
